<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cars extends CI_Controller {

	public function __construct() {
        
        parent::__construct();
        $this->load->library('smartyciclass');
        $this->load->helper('url');

     
        
    }

	public function index()
	{

	    #load smarty template display	
        $this->smartyciclass->display('template/header.tpl');     
		$this->smartyciclass->display('cars/cars.tpl'); 
        $this->smartyciclass->display('template/footer.tpl');   
	}
}