
<!-- this page use smarty template all tags '<?php' not use more -->

<input type="hidden" name="url" id="url" value="{'http://localhost/apiestadao/index.php/api/cars/lastReg' }">
<input type="hidden" name="url_update" id="url_update" value="{'http://localhost/apiestadao/index.php/api/cars/update' }">
<input type="hidden" name="url_add" id="url_add" value="{'http://localhost/apiestadao/index.php/api/cars/new/' }">
<input type="hidden" name="url_find" id="url_find" value="{'http://localhost/apiestadao/index.php/api/cars/find/' }">
<input type="hidden" name="url_delete" id="url_delete" value="{'http://localhost/apiestadao/index.php/api/cars/delete/' }">

<input type="hidden" name="url_list" id="url_list" value="{'http://localhost/apiestadao/index.php/api/cars/list' }">

</br>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">

				<div class="ibox-content">


					<div class="table-responsive" style="overflow-x: hidden">
						<!--<a href="#" id="btn-add" class="btn btn-primary "><i class="fa fa-plus label-bt-add"></i>&nbsp;Novo Arquivo</a>-->

						<button type="button" class="btn btn-primary btmodalShow"  style="position:relative;background-color:#1c84c6;border:none"  data-toggle="modal" data-target="#modalShowModal" title="Adicionar Registro"><i class="fa fa-plus label-bt-add" ></i>  Adicionar Veículos</button>



						<div class="pull-right">
                            <h5 class="no-margins text-right "  ><p class="h5t"></p> </h5>

                        </div>



						<table class="table table-striped table-bordered table-hover dataTables-example" >
							<thead>
							<tr>

								<th>#Id</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Ano</th>
								<th>Ações</th>

							</tr>
							</thead>
							<tbody id="treg">
								
							</tbody>

						</table>
					</div>
					<!--<label name="label-second-plan" id="label-second-plan">0</label>-->
				</div>
			</div>
		</div>
	</div>


</div>

<!-- Modal Incident created André 2019-->
<div class="modal fade"  id="modalShowModal"   aria-labelledby="Inserir"  >
  <div class="modal-dialog"  >
		        <div class="modal-content"  >
		            <div class="modal-header div-close-process"  >
		                <button type="button" class="md-close close bt-close-modal" data-dismiss="modal" aria-hidden="true">×</button>
		                <h4 class="modal-title">Adicionar Veículo</h4>
		            </div>


		            <form name="formadd" id="formadd"  >
						<input type="hidden" name="id" id="id">


				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Marca</label><br>
						            <select class=" " name="select-marca" id="select-marca"   >
						            	 <option selected  value="Chevrolet">Chevrolet</option>
						            	 <option   value="Volkswagen">Volkswagen</option>
						            	 <option   value="Honda">Honda</option> 
						            </select>
						        </div>	

						        <div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Modelo</label><br>
						            <input type="type"  id="modelo" name="modelo"> 
						        </div>								        	       

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Ano</label><br>
						            <input type="type"  id="ano" name="ano"> 
						        </div>	


				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id"></label><br>

						        </div>

						      
			            <div class="modal-footer">
			                <button type="button" id="bt-yes" class="btn btn-sm btn-success bt-addReg " >Gravar</button>
			                <button type="button" class="btn btn-sm btn-success bt-no md-close" data-dismiss="modal">Fechar</button>
			            </div>

			    	</form>
		        </div>
		    </div>
	</div>


<!-- Modal Update created André 2019-->
<div class="modal fade"  id="modalUpdate"   aria-labelledby="Inserir"  >
  <div class="modal-dialog"  >
		        <div class="modal-content"  >
		            <div class="modal-header div-close-process"  >
		                <button type="button" class="md-close close bt-close-modal" data-dismiss="modal" aria-hidden="true">×</button>
		                <h4 class="modal-title">Editar Veículo</h4>
		            </div>


		            <form name="formadd" id="formadd"  >
						<input type="hidden" name="edit-id" id="edit-id">


				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Marca</label><br>
						            <select class=" " name="edit-select-marca" id="edit-select-marca"   >
						            	 <option selected  value="Chevrolet">Chevrolet</option>
						            	 <option   value="Volkswagen">Volkswagen</option>
						            	 <option   value="Honda">Honda</option> 
						            </select>
						        </div>	

						        <div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Modelo</label><br>
						            <input type="type"  id="edit-modelo" name="edit-modelo"> 
						        </div>								        	       

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Ano</label><br>
						            <input type="type"  id="edit-ano" name="edit-ano"> 
						        </div>	


				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id"></label><br>

						        </div>

						      
			            <div class="modal-footer">
			                <button type="button" id="bt-yes" class="btn btn-sm btn-success bt-update " >Gravar</button>
			                <button type="button" class="btn btn-sm btn-success bt-no md-close" data-dismiss="modal">Fechar</button>
			            </div>

			    	</form>
		        </div>
		    </div>
	</div>

	<script>
    {literal}

    // list cars - created André 03/10/2019
    var rows;
    var idreg="";


 		$.ajax({
		                    cache:false,
		                    type: 'GET',
		                    url: $("#url_list").val() ,
		                    dataType: "json",
		                    success: function(data) {

		                    	//console.log(data["list"]);return;

		                    if(data.success){

				                 	$.each(data["list"], function (i, value) {

				                 		
											rows = rows + "<tr >";
										   	rows = rows + "<td  data-id='"+value.id+"'>"+value.id +"</td>";
											rows = rows + "<td  data-error='"+value.marca +"'>"+value.marca+"</td>";
											rows = rows + "<td  data-error='"+value.modelo +"'>"+value.modelo+"</td>";
											rows = rows + "<td  data-error='"+value.ano +"'>"+value.ano+"</td>";
											rows = rows + "<td  style='text-align: center'><a href='#' class='md-trigger'   ><button type='button' class='btn btn-success btn-xs bt-edit-modal' data-id='"+value.id+"' data-toggle='modal' data-target='#modalUpdate' data-placement='right' title='Salvar'><i class='fa fa-pencil'></i></button> <button type='button' class='btn btn-danger btn-xs bt-del' id='btdel' data-id='"+value.id+"'  data-toggle='modal' data-target='#modalDelete' data-placement='right' title='Excluir'><i class='fa fa-times'></i></button></td>"
											rows = rows + "</tr>";						
											

											

										   	i=i+1;
									});


				                 	$(".h5t").text(data['count'] + ' registro(s) encontrados(s)')	
				                 	$("#treg").append(rows);
							
								}
							}		
		});



		 $(".btmodalShow").on('click',function() {


				 	//---------------------------initialize AJAX GET last register
				 $.ajax({
		                    cache:false,
		                    type: 'GET',
		                    url: $("#url").val() ,
		                    dataType: "json",
		                    success: function(data) {

		                    	//insert last key reg bd
		                    	$("#id").val(data);

		                    }


		         });


		 })

		 $(".bt-addReg").on("click",function(){

		 		//get values
		 		var id       = $("#id").val();
		 		var marca    = $("#select-marca").val();
		 		var modelo   = $("#modelo").val();
				var ano      = $("#ano").val();



 				$.ajax({
		                    cache:false,
		                    type: 'GET',
		                    url: $("#url").val() ,
		                    dataType: "json",
		                    success: function(data) {

		                    	//get last reg
		                    	idreg = parseInt(data.query) + parseInt(1);


								//validation
							  		if(idreg=="")
							  			id = 0;
							  		else
							  			id=idreg;
							  		

							  		if(marca==""){
							  			jAlert('marca');
							  			return;
							  		}

							  		if(modelo==""){
							  			jAlert('modelo');
							  			return;
							  		}

							  		if(ano==""){
							  			jAlert('ano');
							  			return;
							  		}

					  		
							  								 				 		
							  		var rows="";
							  		//var data = [];

							  		//data.push({id:id,marca:marca,modelo:modelo,ano:ano});


							  		  //if ok insert
									 //---------------------------initialize AJAX GET SAVE
									 $.ajax({
							                    cache:false,
							                    type: 'POST',
							                    url: $("#url_add").val(),
							                    data: {id:id,marca:marca,modelo:modelo,ano:ano},
							                    dataType: "json",
							                    success: function(data) {


							                    	if(data.success){
								

														var btn  = $(this),
													    show = btn.data('show'),
													    hide = btn.data('hide');


																$.jAlert({

																	'title':'Sucesso',
																	'content':'Cadastrado com sucesso recarregando aguarde ...',
															        'theme': 'green',
															        'showAnimation' : show,
															        'hideAnimation' : hide,
															        'btns': { 'text': 'Fechar' },
														                'onOpen': function(alert){
																							                    
													                    window.setTimeout(function(){

														                    	alert.closeAlert();
														                    	alt=1;
														                    	location.reload();	
														                    	}, 1000);
														                }
														     	 });



							                    	}


							                    }


							         });





							}

				})
		 		 		
		 		


		 })		

		 $(document).on('click', '.bt-edit-modal', function() {
		 	$("#edit-id").val($(this).data('id')) ;

		 	//find
	 		$.ajax({
			                    cache:false,
			                    type: 'GET',
			                    url: $("#url_find").val() + $(this).data('id') ,
			                    dataType: "json",
			                    success: function(data) {



			                    if(data.success){

										$("#edit-select-marca").val(data["list"][0].marca) ;
										$("#edit-modelo").val(data["list"][0].modelo) ;
										$("#edit-ano").val(data["list"][0].ano) ;								
									}
								}		
			});		 	

		 	
		 })

		 $(document).on('click', '.bt-update', function() {



		 		//get values
		     	var id       = $("#edit-id").val();
		 		var marca    = $("#edit-select-marca").val();
		 		var modelo   = $("#edit-modelo").val();
				var ano      = $("#edit-ano").val();


				//POR SE TRATAR DE UM BD TXT o update foi feito dessa forma. 

				 $.ajax({
	                    cache:false,
	                    type: 'DELETE',
	                    url: $("#url_delete").val()+id ,
	                    dataType: "json",
	                    success: function(data) {
						
						//jalert
						var btn  = $(this),
					    show = btn.data('show'),
					    hide = btn.data('hide');
						    

	                      if(data.success){	
	                      		//first delete successs

									 $.ajax({
							                    cache:false,
							                    type: 'POST',
							                    url: $("#url_add").val(),
							                    data: {id:id,marca:marca,modelo:modelo,ano:ano},
							                    dataType: "json",
							                    success: function(data) {


							                    	if(data.success){
								

														var btn  = $(this),
													    show = btn.data('show'),
													    hide = btn.data('hide');


																$.jAlert({

																	'title':'Sucesso',
																	'content':'Editado com sucesso recarregando aguarde ...',
															        'theme': 'green',
															        'showAnimation' : show,
															        'hideAnimation' : hide,
															        'btns': { 'text': 'Fechar' },
														                'onOpen': function(alert){
																							                    
													                    window.setTimeout(function(){

														                    	alert.closeAlert();
														                    	alt=1;
														                    	location.reload();	
														                    	}, 1000);
														                }
														     	 });



							                    	}


							                    }


							         });	                      	


						  }else{

							$.jAlert({

								'title':'Erro!',
								'content':data.message,
						        'theme': 'red',
						        'showAnimation' : show,
						        'hideAnimation' : hide,
						        'btns': { 'text': 'Fechar' },
					                'onOpen': function(alert){
														                    
				                    window.setTimeout(function(){

					                    	alert.closeAlert();
					                    	alt=1;
					                    	location.reload();	
					                    	}, 1000);
					                }
					     	 });

						  }

	                    }


	         });





		 })


		 $(document).on('click', '.bt-del', function() {


		 	var id = $(this).data("id");

		 	//---------------------------initialize AJAX GET last register
			 $.ajax({
	                    cache:false,
	                    type: 'DELETE',
	                    url: $("#url_delete").val()+id ,
	                    dataType: "json",
	                    success: function(data) {


	                      if(data.success){	


							var btn  = $(this),
						    show = btn.data('show'),
						    hide = btn.data('hide');
						    
							$.jAlert({

								'title':'Sucesso',
								'content':data.message,
						        'theme': 'green',
						        'showAnimation' : show,
						        'hideAnimation' : hide,
						        'btns': { 'text': 'Fechar' },
					                'onOpen': function(alert){
														                    
				                    window.setTimeout(function(){

					                    	location.reload();	
					                    	}, 1000);
					                }
					     	 });

						  }

	                    }


	         });

		 })


		 function jAlert(field){


			var btn  = $(this),
		    show = btn.data('show'),
		    hide = btn.data('hide');


					$.jAlert({

						'title':'Oppssss!',
						'content':'O campo '+ field + ' está vazio',
				        'theme': 'red',
				        'showAnimation' : show,
				        'hideAnimation' : hide,
				        'btns': { 'text': 'Fechar' },
			                'onOpen': function(alert){
			                    window.setTimeout(function(){
			                    	alert.closeAlert();
			                    	alt=1;
			                    	}, 3000);
			                }
			     	 });

			return;


		 }


	{/literal}	 



	</script>